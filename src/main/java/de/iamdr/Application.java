package de.iamdr;

import de.iamdr.services.PropertyCompareStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.HandlerExceptionResolver;

@SpringBootApplication
public class Application {

private final static Logger LOGGER = LogManager.getLogger(Application.class);

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  public HandlerExceptionResolver sentryExceptionResolver() {
    return new io.sentry.spring.SentryExceptionResolver();
  }

  /*
   * Hook on the requests.
   */
  @Bean
  public ServletContextInitializer sentryServletContextInitializer() {
    return new io.sentry.spring.SentryServletContextInitializer();
  }

  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx, PropertyCompareStorageService storageService) {
    return args -> {
      storageService.deleteAll();
      storageService.init();
      if(LOGGER.isInfoEnabled()) {
        LOGGER.info("Let's go!");
      }
    };
  }

}

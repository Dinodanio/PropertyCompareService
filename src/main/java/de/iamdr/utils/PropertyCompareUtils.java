package de.iamdr.utils;

import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyCompareUtils {
  public static Map<String, Object> compareProperties(final String candidatePath, final String referencePath) throws IOException {

    final Properties referenceProperties = new Properties();
    final Properties candidateProperties = new Properties();

    try (final InputStream referenceStream = new FileInputStream(referencePath)) {
      try (final InputStream candidateStream = new FileInputStream(candidatePath)) {
        referenceProperties.load(referenceStream);
        candidateProperties.load(candidateStream);
      }
    }

    final MapDifference<?, ?> mapDifference = Maps.difference(candidateProperties, referenceProperties);

    final Map<String, Object> result = new HashMap<>();
    result.put("onlyInCandidate", mapDifference.entriesOnlyOnLeft().entrySet());
    result.put("onlyInReference", mapDifference.entriesOnlyOnRight().entrySet());
    result.put("diffValues", mapDifference.entriesDiffering().entrySet());

    return result;
  }
}
